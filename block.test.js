const Block = require('./block');
const Constants = require('./config.js');
const cryptoHash = require('./crypto-hash');
describe('Block Class', () => {
	const timestamp = 'a-date'
	, lastHash = 'dummy'
	, hash = 'hashing'
	, data = "data"
	, block = new Block({timestamp, lastHash, hash, data});

	it("has a timestamp, lastHash, hash, and data property", () =>{
		expect(block.timestamp).toEqual(timestamp);
		expect(block.lastHash).toEqual(lastHash);
		expect(block.data).toEqual(data);
		expect(block.hash).toEqual(hash);
	});
});
describe("Genesis Block", ()=>{
	const genesisBlock = Block.genesis();
	it("is an instance of block", () => {
		expect(genesisBlock instanceof Block).toEqual(true);
	});
	it("has the default values", ()=>{
		expect(genesisBlock.timestamp).toEqual(Constants.GENESIS_DATA.timestamp);
		expect(genesisBlock.lastHash).toEqual(Constants.GENESIS_DATA.lastHash);
		expect(genesisBlock.hash).toEqual(Constants.GENESIS_DATA.hash);
		expect(genesisBlock.data).toEqual(Constants.GENESIS_DATA.data);
		expect(genesisBlock).toEqual(Constants.GENESIS_DATA);
	});
describe("Mine Block", ()=>{
	const lastBlock = Block.genesis();
	const data = "mined";
	const newBlock = Block.mine({lastBlock, data});
	it("is an instance of block", ()=>{
		expect(newBlock instanceof Block).toEqual(true);
	});
	it("has necessary data", ()=>{
		expect(newBlock.data).toEqual(data);
	});
	it("references the previous block", ()=>{
		expect(newBlock.lastHash).toEqual(lastBlock.hash);
	});
	it("has a hash", ()=>{
		expect(newBlock.hash).not.toEqual(undefined);
	})
	it("has the SHA-256 hash", ()=>{
		expect(newBlock.hash).toEqual(cryptoHash(newBlock.timestamp, lastBlock.hash, data));
	});
});
});