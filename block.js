const Constants = require('./config');
const cryptoHash = require('./crypto-hash');
class Block
{
	constructor({timestamp, lastHash, hash, data})
	{
		this.timestamp = timestamp;
		this.lastHash = lastHash;
		this.hash = hash;
		this.data = data;
	}
	static genesis()
	{
		return new Block({timestamp: Constants.GENESIS_DATA.timestamp,
			lastHash: Constants.GENESIS_DATA.lastHash,
			hash: Constants.GENESIS_DATA.hash,
			data: Constants.GENESIS_DATA.data});
	}
	static mine({lastBlock, data})
	{
		const lastHash = lastBlock.hash;
		const timestamp = Date.now();
		return new Block({data, lastHash, timestamp, hash: cryptoHash(data, lastHash, timestamp)});
	}
}
// const block1 = new Block({timestamp: "1/1/2018", lastHash: "walter", hash: "cwalter", data: "nipun"});
// console.log(block1);

module.exports = Block;
