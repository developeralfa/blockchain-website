const Block = require('./block');
const cryptoHash = require('./crypto-hash');
class Blockchain
{
	constructor()
	{
		this.chain = [Block.genesis()];
	}
	addBlock({data})
	{
		const timestamp =  Date.now()
		const lastHash = this.chain[this.chain.length-1].hash
		const b = new Block({timestamp, lastHash, data, hash: cryptoHash(lastHash, data, timestamp)});
		this.chain.push(b);
	}
	static validate(chain)
	{
		if(chain.length == 0) return false;
		if(JSON.stringify(chain[0])!=JSON.stringify(Block.genesis())) return false;
		for(let i = 1; i < chain.length; i++)
		{
			if(chain[i].lastHash != chain[i-1].hash) return false;
			if(chain[i].hash != cryptoHash(chain[i-1].hash, chain[i].timestamp, chain[i].data)) return false;
		}
		return true;
	}
	replaceChain(that)
	{
		//console.log(that.chain);
		if(!Blockchain.validate(that.chain)) return;
		if(that.chain.length > this.chain.length)
		{
			this.chain = that.chain.slice();
		}
	}
};

var block

module.exports = Blockchain;