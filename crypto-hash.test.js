const cryptoHash = require('./crypto-hash');

describe("cryptoHash", ()=>{
	it("creates a SHA-256 encoding", ()=>{
		expect(cryptoHash('foo')).toEqual("2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae");
	});

	it("produces the same hash for different order", ()=>{
		expect(cryptoHash('abc','bcd','cde')).toEqual(cryptoHash('abc', 'cde', 'bcd'));
	});
})