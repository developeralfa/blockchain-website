const Blockchain = require('./blockchain');
const Block = require('./block');
describe("Blockchain", ()=>{
	const blockchain = new Blockchain();
	const data = "wow";
	it("is an instance of a Blockchain", ()=>{
		expect(blockchain instanceof Blockchain).toEqual(true);
	});
	it("has a size of 1", ()=>{
		expect(blockchain.chain.length).toEqual(1);
	});
	it("has a genesis block", ()=>{
		expect(blockchain.chain[0]).toEqual(Block.genesis());
	});
	it("adds a new block", ()=>{
		blockchain.addBlock({data});
		expect(blockchain.chain[blockchain.chain.length-1].data).toEqual(data);
	expect(blockchain.chain.length).toEqual(2);
	});
});

describe("Validate chain", ()=>{
	const blockchain = new Blockchain();
	it("is valid when of length 1", ()=>{
		expect(Blockchain.validate(blockchain.chain)).toEqual(true);
	});
	blockchain.addBlock({data: "finger"});
	it("is valid when of length 2", ()=>{
		expect(Blockchain.validate(blockchain.chain)).toEqual(true);
	});
	blockchain.addBlock({data: "thumb"});
	it("is valid when of length 3", ()=>{
		expect(Blockchain.validate(blockchain.chain)).toEqual(true);
	});
});

describe("Replace Chain", ()=>{
	const b1 = new Blockchain();
	const b2 = new Blockchain();
	describe("The chain is not longer", ()=>{
		b1.addBlock({data: "data1"});
		//console.log(b1);
		it("shouldn't replace the chain", ()=>{
			b1.replaceChain(b2);
			expect(b1.chain).not.toEqual(b2.chain);
		});
	});
	describe("The chain is longer", ()=>{
		const b3 = new Blockchain();
		b3.addBlock({data: "1"});
		b3.addBlock({data: "2"});
		describe("The chain is invalid", ()=>{
			const b4 = new Blockchain();
			b4.chain[0].data = "NULL";
			b4.addBlock({data: "1"});
			b4.addBlock({data: "2"});
			it("does not replace the chain", ()=>{
				b1.replaceChain(b4);
				expect(b1.chain).not.toEqual(b4.chain);
			});
		});
		describe("The chain is valid", ()=>{
			it("replaces the chain", ()=>{
				b1.replaceChain(b3);
				expect(b1.chain).toEqual(b3.chain);
			});
		});
	});
});